#!/usr/bin/env ruby

require 'rubygems'
require 'gollum/app'

gollum_path = File.expand_path(File.dirname(__FILE__))

Precious::App.set(:gollum_path,   "#{gollum_path}/data")
Precious::App.set(:default_markup, :markdown)
Precious::App.set(:wiki_options, {
  :template_dir  => "#{gollum_path}/templates",
  :live_preview  => false,
  :allow_uploads => false,
  :mathjax       => false,
  :h1_ttile      => true,
  :universal_toc => false,
})

Precious::App.settings.mustache[:templates] = "#{gollum_path}/templates"

run Precious::App
