# Dockerfile for gollum
# Version: 1.0.0

FROM        ubuntu:14.04
MAINTAINER  Naoki OKAMURA (Nyarla) <nyarla@thotep.net>

ENV DEBIAN_FRONTEND noninteractive

RUN apt-get update
RUN apt-get upgrade -y

RUN adduser --group app
RUN useradd -m -g app app

RUN apt-get install -y git

RUN apt-get install -y software-properties-common
RUN add-apt-repository ppa:brightbox/ruby-ng
RUN apt-get update

RUN apt-get install -y ruby2.1 ruby2.1-dev build-essential sqlite3 libsqlite3-dev libicu-dev libz-dev libssl-dev

RUN gem install gollum puma --no-ri --no-rdoc

RUN mkdir -p /opt/gollum
ADD templates /opt/gollum/templates

ADD run /opt/gollum/run
ADD gloria.ru /opt/gollum/gloria.ru
RUN chmod +x /opt/gollum/run
RUN chown -R app:app /opt

EXPOSE 8080
USER app
ENTRYPOINT [ "/opt/gollum/run" ]


