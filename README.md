Gloria - The Customized gollum template toolkit
===============================================

SCREENSHOT
----------

![Screenshot of Gloria](https://raw.githubusercontent.com/nyarla/gloria/master/screenshot.png)

SYNOPSIS
--------

In CoreOS:

```
$ mkdir -p ~/services && cd ~/services
$ git clone git://github.com/nyarla/gloria
$ cd gloria
$ docker build -t nyarla/gloria .
$ sudo docker run --name gloria -p 0.0.0.0:80:8080 -v /home/core/data/gollum:/opt/gollum/data nyarla/gloria 
```

DESCRIPTION
-----------

This package is a customized [gollum](https://github.com/gollum/gollum) templates toolkit.

MODIFIER
--------

Naoki OKAMURA (Nyarla) <nyarla@thotep.net>

LICENSE
-------

See [LICENSE.md](https://github.com/nyarla/gloria/blob/master/LICENSE.md)
